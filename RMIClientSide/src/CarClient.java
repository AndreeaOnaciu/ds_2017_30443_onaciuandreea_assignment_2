
import java.rmi.*;

 
public class CarClient {
	public static void main (String[] args) {
		CarInterface carInterface;
		CarController carController;
		try {
			carInterface = (CarInterface)Naming.lookup("rmi://localhost/ABC");
			/*System.out.println("Price value: "
					+ carInterface.computePriceSelling(new Car(2009,2000,200.000)));
			System.out.println("Tax value: "
					+ carInterface.computeTax(new Car(2009, 2000)));
			*/
			carController=new CarController(carInterface);
 
			}catch (Exception e) {
				System.out.println("HelloClient exception: " + e);
				}
		}
}