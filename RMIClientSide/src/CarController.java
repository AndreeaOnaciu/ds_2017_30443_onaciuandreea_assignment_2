
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;


public class CarController {
	private CarView carView;
	private CarInterface carInterface;

	public CarController(CarInterface carInterface) {
		this.carInterface = carInterface;
		carView = new CarView();
		carView.setVisible(true);

		carView.addBtnComputePriceActionListener(new ComputePriceActionListener());
		carView.addBtnComputeTaxActionListener(new ComputeTaxActionListener());
	}

	class ComputePriceActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			String purchasingPrice = carView.getPurchasingPrice();
			String year = carView.getYear();
			double price = Double.valueOf(purchasingPrice);
			Car c = new Car();
			c.setPricePurchasing(price);
			c.setYear(Integer.valueOf(year));
			try {
				double result = carInterface.computePriceSelling(c);
				carView.printResultPrice(result);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	class ComputeTaxActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			String engineSize = carView.getEngineSize();
			int capacity = Integer.valueOf(engineSize);
			Car c = new Car();
			c.setEngineCapacity(capacity);
			try {
				double result = carInterface.computeTax(c);
				carView.printResultTax(result);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
