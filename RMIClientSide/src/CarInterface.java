
import java.rmi.Remote;
import java.rmi.RemoteException;



public interface CarInterface extends Remote {

	/**
	 * Computes the selling price for a Car.
	 *
	 * @param c Car for which to compute the selling price
	 * @return selling price for the car
	 * @throws RemoteException 
	 */
	double computePriceSelling(Car c) throws RemoteException;
	
	/**
	 * Computes the tax to be payed for a Car.
	 *
	 * @param c Car for which to compute the tax
	 * @return tax for the car
	 * @throws RemoteException 
	 */
	double computeTax(Car c) throws RemoteException;
}
