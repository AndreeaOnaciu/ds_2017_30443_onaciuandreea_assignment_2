
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class CarView extends JFrame {

	private JPanel contentPane;
	private JTextField engineSize;
	private JTextField year;
	private JTextField purchasingPrice;
	private JButton btnComputeTax;
	private JButton btnComputePrice;
	private JTextArea textAreaTax;
	private JTextArea textAreaPrice;
	
	public CarView() {
		setTitle("Car RMI project");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300,100,450,300);
		contentPane=new JPanel();
		contentPane.setBorder(new EmptyBorder(5,5,5,5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblInsertCarInfo=new JLabel("Insert your car information");
		lblInsertCarInfo.setBounds(10,11,180,14);
		contentPane.add(lblInsertCarInfo);
		
		JLabel lblEngineSize = new JLabel("Engine Size");
		lblEngineSize.setBounds(10, 36, 100, 14);
		contentPane.add(lblEngineSize);

		JLabel lblYear = new JLabel("Year");
		lblYear.setBounds(10, 61, 100, 14);
		contentPane.add(lblYear);

		JLabel lblPurchasingPrice = new JLabel("Purchasing Price");
		lblPurchasingPrice.setBounds(10, 86, 100, 14);
		contentPane.add(lblPurchasingPrice);
		
		engineSize = new JTextField();
		engineSize.setBounds(120, 33, 86, 20);
		contentPane.add(engineSize);
		engineSize.setColumns(10);

		year = new JTextField();
		year.setBounds(120, 58, 86, 20);
		contentPane.add(year);
		year.setColumns(10);

		purchasingPrice = new JTextField();
		purchasingPrice.setBounds(120, 83, 86, 20);
		contentPane.add(purchasingPrice);
		purchasingPrice.setColumns(10);

		btnComputeTax = new JButton("Tax");
		btnComputeTax.setBounds(10, 132, 89, 23);
		contentPane.add(btnComputeTax);		
		
		btnComputePrice = new JButton("Price");
		btnComputePrice.setBounds(10, 160, 89, 23);
		contentPane.add(btnComputePrice);
		
		textAreaTax = new JTextArea();
		textAreaTax.setBounds(110, 131, 171, 23);
		contentPane.add(textAreaTax);
		
		textAreaPrice = new JTextArea();
		textAreaPrice.setBounds(110, 160, 171, 23);
		contentPane.add(textAreaPrice);
	}
	
	public void clear() {
		engineSize.setText("");
		purchasingPrice.setText("");
		year.setText("");
		textAreaPrice.setText("");
		textAreaTax.setText("");
	}

	
	public void addBtnComputeTaxActionListener(ActionListener e) {
		btnComputeTax.addActionListener(e);
	}
	
	public void addBtnComputePriceActionListener(ActionListener e) {
		btnComputePrice.addActionListener(e);
	}
	
	public String getEngineSize() {
		return engineSize.getText();
	}
	
	public String getYear() {
		return year.getText();
	}
	
	public String getPurchasingPrice() {
		return purchasingPrice.getText();
	}
	
	public void printResultTax(double result) {
		textAreaTax.setText(String.valueOf(result));
	}

	public void printResultPrice(double result) {
		textAreaPrice.setText(String.valueOf(result));
	}
}
