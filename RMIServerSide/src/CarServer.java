
import java.rmi.*;
import java.rmi.server.*;   
 
public class CarServer {
	   public static void main (String[] argv) {
		   try {
			   CarService service = new CarService();			   		   
			   Naming.rebind("rmi://localhost/ABC", service);
 
			   System.out.println("Car Server is ready.");
			   }catch (Exception e) {
				   System.out.println("Car Server failed: " + e);
				}
		   }
}