
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;



public class CarService extends UnicastRemoteObject implements CarInterface{
	
	 public CarService() throws RemoteException {   }
	 
	@Override
	public double computePriceSelling(Car c) throws RemoteException {
		double priceS=c.getPricePurchasing()-(c.getPricePurchasing()/7)*(2015-c.getYear());
		return priceS;
	}
	
	public double computeTax(Car c) throws RemoteException{
		// Dummy formula
		if (c.getEngineCapacity() <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		int sum = 8;
		if(c.getEngineCapacity() > 1601) sum = 18;
		if(c.getEngineCapacity() > 2001) sum = 72;
		if(c.getEngineCapacity() > 2601) sum = 144;
		if(c.getEngineCapacity() > 3001) sum = 290;
		return c.getEngineCapacity() / 200.0 * sum;
	}
}
